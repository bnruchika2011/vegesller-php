<?php

include 'classes/dbconnect.php';

class purchase extends dbconnect{

public function select($sql, $cond=null){
        $con = new dbconnect();
        
        $result = false;
        try{
            $con->stmt = $con->pdo->prepare($sql);
            $con->stmt->execute($cond);
            $result = $con->stmt->fetchAll();
        }   catch (Exception $ex) {die($ex->getMessage()); }
        $con->stmt = null;
       return $result;        
    }
    
public function getItems(){
    return $this->select("SELECT * FROM `daily_purchase`");
      }
    
//     INSERTION
public function insert($sql, $cond=null){
   
   $con = new dbconnect();
   
    try{   
        $item = $_POST['item'];
        $quantity = $_POST['quantity'];
        $buy_price = $_POST['buy_price'];
        $sell_price = $_POST['sell_price'];
        $dt = $_POST['dt'];
        
        $query = $con->pdo->prepare($sql);
        
        $query->bindParam(':item', $item);
        $query->bindParam(':quantity', $buy_price);
        $query->bindParam(':buy_price', $buy_price);
        $query->bindParam(':sell_price', $sell_price);
        $query->bindParam(':dt', $dt);
        
       $query->execute($cond);
       
        }catch (Exception $ex) { die($ex->getMessage()); }
}
    
    public function insertItem(){
        return $this->insert("INSERT into `daily_purchase` (item, quantity, buy_price, sell_price, dt) VALUES(:item, :quantity, :buy_price, :sell_price, :dt)");
    }
 
 // DELETION 

 public function deleteItem($sql, $cond=null){
    
    $con = new dbconnect();
    $id= $_GET['id'];
    try{
        $id = $_GET['delete'];
        $query = $con->pdo->prepare($sql);
        $query->execute(array(':id'=> $id));
    }catch (Exception $ex){ die($ex->getMessage()); }
 }
 
 public function delete(){
    return $this->deleteItem("Delete from `daily_purchase` where id=:id");
 }
}

?>
