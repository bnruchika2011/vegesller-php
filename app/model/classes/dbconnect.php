<?php 
// this is equal to config.php
class dbconnect{

public $pdo = null;
public $stmt = null;

function __construct(){
    try{
        $this->pdo = new PDO("mysql:host=localhost;dbname=vegeseller;charset=utf8", "root", "1",
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,]
        );
    }   catch(Exception $e) {die($e->getMessage()); }
}

function __destruct(){
    if($this->stmt != null) {$this->stmt = null;}
    if($this->pdo != null) {$this->pdo = null; }
}

}

?>
