<?php 
include 'classes/dbconnect.php';

class selling extends dbconnect{

    public function select($sql, $cond=null){
        $con = new dbconnect();
        
        $result = false;
        try{
            $con->stmt = $con->pdo->prepare($sql);
            $con->stmt->execute($cond);
            $result = $con->stmt->fetchAll();
        }   catch (Exception $ex) {die($ex->getMessage()); }
        $con->stmt = null;
       return $result;
        
    }
     public function getVege(){
        return $this->select("SELECT * FROM `daily_purchase`");
      }
     public function get(){
        return $this->select("SELECT * FROM `selling`");
    }
    
    public function insert($sql, $cond=null){
   
        $con = new dbconnect();
   
        try{   
            $street = $_POST['street'];
            $item = $_POST['veges'];
            $quantity = $_POST['vege-qut'];
            
            $query = $con->pdo->prepare($sql);
            
            $query->bindParam(':street', $street);
            $query->bindParam(':item', $item);
            $query->bindParam(':quantity', $quantity);            
            $query->execute($cond);
            }catch (Exception $ex) { die($ex->getMessage()); }
}
    
    
       public function put(){
        return $this->insert("INSERT into `selling` (street, item, quantity) VALUES(:street, :item, :quantity)");
    }
   
}
