<?php


class purchaseController extends Controller{

    public function myPurchase(){
        $this->model('purchase');
        $data = $this->model->get();
        $this->view('seller' . DIRECTORY_SEPARATOR . 'myPurchase');
        $this->view->page_title = 'My Purchases';
        $this->view->render();
        
    }
    
}
?>
