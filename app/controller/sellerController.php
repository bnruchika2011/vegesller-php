<?php 

class sellerController extends Controller{
    
public function myPurchase(){
        $this->model('purchase');
        
        if(isset($_POST['save'])){
            $this->view('seller' . DIRECTORY_SEPARATOR . 'myPurchase', [ $this->model->put() ]);
        }
        
        
        if(isset($_GET['delete'])){
            $this->view('seller' . DIRECTORY_SEPARATOR . 'myPurchase', [  $this->model->del()]);
            
        }
        
        $this->view('seller' . DIRECTORY_SEPARATOR . 'myPurchase', ['items' => $this->model->get()]);
        
        $this->view->page_title = 'My Purchases';
        
        $this->view->render();
}

public function startSelling(){
        $this->model('selling');
        
        $this->view('seller' . DIRECTORY_SEPARATOR . 'startSelling', [ 'item' => $this->model->get()]);
            
        if(isset($_POST['save_street'])){
                $this->view('seller' . DIRECTORY_SEPARATOR . 'startSelling', [  $this->model->put()]);
                }
                
        $this->view('seller' . DIRECTORY_SEPARATOR . 'startSelling', [ 'items' => $this->model->getVege()]);

        $this->view->page_title = 'Start Selling';
        $this->view->render();
}

public function sellingDetails(){
        $this->model('seller');
        $this->view('seller' . DIRECTORY_SEPARATOR . 'sellingDetails');
        $this->view->page_title = 'Selling Details';
        $this->view->render();
}

public function growBusiness(){
        $this->model('seller');
        $this->view('seller' . DIRECTORY_SEPARATOR . 'growBusiness');
        $this->view->page_title = 'Grow Business';
        $this->view->render();
}
}
