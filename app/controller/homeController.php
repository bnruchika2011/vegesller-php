<?php

include 'core/Controller.php';


class homeController extends Controller{

public function index($id='',$name=''){
   
    $this->view('home/index',[
    'name' => $name,
    'id' =>  $id
    ]);
    $this->view->page_title = 'Home';
    //var_dump($this);
    $this->view->render();
}

public function aboutUs($id='',$name=''){
    $this->view('home/aboutUs',[
    'name' => $name,
    'id' =>  $id
    ]);
      $this->view->page_title = 'About';
    $this->view->render();
}
}

?>
